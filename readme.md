# Personal suckless dwm config.h

- [Info](#info)
- [WARNING](#warning)
- [keybindings](#keybindings)

# Info

- What is dwm ?
    - dwm is a dynamic window manager for X. It manages windows in tiled, monocle and floating layouts. All of the layouts can be applied dynamically, optimising the environment for the application in use and the task performed.
    - dwm is programmed in C, so you need to compile the source everytime you make a change.
    - [more info](https://dwm.suckless.org/)
- Why do I use it ?
    - It is simple [(2000 lines of code more or less)](https://git.suckless.org/dwm/files.html)
    - It works (Compile it and run it on X server start, that's it)
- [Default dwm guide](https://ratfactor.com/dwm)

# WARNING

- The default keys have been altered, [you can see the keybindings with the default programs in the next paragraph](#keybindings).
- The default programs I use are set in lines 60, 61, and 62 in [config.h](/../../blob/main/config.h).
    - 55 [app menu](https://tools.suckless.org/dmenu/)
    - 56 [terminal](https://st.suckless.org/)
    - 57 web browser (xdg-open)
    - 58 [virtualbox](https://www.virtualbox.org/)
    - 59 [keepassxc](https://keepassxc.org/)

# keybindings

- Mod is Super

## app

| Action              |                                                   | Key               |
|---------------------|---------------------------------------------------|-------------------|
| Open app menu       | [dmenu](https://tools.suckless.org/dmenu/)        | Mod + d           |
| Open terminal       | [st](https://st.suckless.org/)                    | Mod + Return      |
| Open web browser    | xdg-open                                          | Mod + o           |
| Open virtualbox     | [virtualbox](https://www.virtualbox.org/)         | Mod + i           |
| Open keepassxc      | [keepassxc](https://keepassxc.org/)               | Mod + u           |

---

## peripherals

| Action                                                 | Key                    |
|--------------------------------------------------------|------------------------|
| Increase volume                                        | Mod + n                |
| Decrease volume                                        | Mod + Shift + n        |
| Mute volume                                            | Mod + Ctrl + n         |
| Mute mic                                               | Mod + z                |

---

## windows movement

| Action                                                      | Key                    |
|-------------------------------------------------------------|------------------------|
| Increase master area                                        | Mod + k                |
| Increase slave area                                         | Mod + j                |
| Change focus to previous window in the same screen          | Mod + h                |
| Change focus to next window in the same screen              | Mod + l                |
| Change focus to previous screen                             | Mod + Shift + h        |
| Change focus to next screen                                 | Mod + Shift + l        |
| Move window to previous screen and set focus to prev. sceen | Mod + Ctrl + h         |
| Move window to next screen and set focus to next screen     | Mod + Ctrl + l         |
| Change window from slave to master area and vice versa      | Mod + Shift + Return   |
